@echo off

git status
if errorlevel 1 (
	git init
	git remote add origin https://rocketgit.com/user/fluffrabbit/SkyHammer
)
git add -A .
set /p desc=Commit description: 
set /p username=Username to display: 
git config user.email "@"
git config user.name "%username%"
git commit -m "%desc%"
git push origin master
if errorlevel 1 (
	echo Forcing push, wiping the repository and commit history and replacing it with your folder. Only proceed if you REALLY want to shake things up. A better alternative might be to run git pull origin master and then try running git-upload again. Proceeding with the force.
	git push origin master --force
)
pause
