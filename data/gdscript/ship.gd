extends RigidBody


onready var target = get_global_transform().origin
onready var safespot = get_tree().get_nodes_in_group( 'safespot' )
onready var player = get_tree().get_nodes_in_group( 'player' )
onready var attentionTimer = randf() * 2.0

var alive = true

var thrust = 9.81635

var personalRadius = 8.0

var attackRadius = 28.0

var attentionSpan = 2.0


func _ready():
	pass


func _integrate_forces( state ):
	var d = state.get_step()
	
	if alive:
		# do something every attentionSpan
		attentionTimer += d
		if attentionTimer >= attentionSpan:
			attentionTimer = 0.0
			ai_do_something()
		
		var pos = get_global_transform().origin
		# go to target
		state.set_linear_velocity( state.get_linear_velocity() + (target - pos).normalized() * thrust * d )
		# fighting with gravity
		state.set_linear_velocity( state.get_linear_velocity() + getlookup( self ) * thrust * d )
		# spin because flying saucers spin
		state.set_angular_velocity( Vector3( 0, -25, 0 ) )


func _pick( body, v, a, e, d ):
	# _pick( body, vec, action { point = 0, start press = 1, start hold = 2, button press = 3  }, actionevent, delta )
	if a > 3:
		ai_go_to_safespot()
		#alive = false


func ai_do_something():
	var choice = randi() % 2
	if choice == 0:
		ai_go_to_safespot()
	else:
		ai_attack_player()


func ai_go_to_safespot():
	attentionTimer = 0.0
	var pos = get_global_transform().origin
	# there must be a safespot closer than this
	var dest = Vector3( 8675309, 8675309, 8675309 )
	# find the closest safespot that is outside of the personalRadius
	for spot in safespot:
		var spotpos = spot.get_global_transform().origin
		var spotdist = spotpos.distance_to( pos )
		if spotdist < dest.distance_to( pos ) and spotdist > personalRadius:
			dest = spotpos
	target = dest


func ai_attack_player():
	var pos = get_global_transform().origin
	# pick a player at random
	var spot = player[ randi() % player.size() ]
	var spotpos = spot.get_global_transform().origin + Vector3( 0.0, 1.0, 0.0 )
	# if player is out of range
	if spotpos.distance_to( pos ) > attackRadius:
		# fly towards player
		target = spotpos
	else:
		# hold position
		target = pos
		# spawn an instance of the plasma
		var plasma = get_node( 'Inventory/Plasma' ).duplicate( true )
		get_tree().get_root().add_child( plasma )
		plasma.show()
		plasma.set_global_transform( get_global_transform() )
		plasma.set_linear_velocity( ( spotpos - pos ).normalized() * 50 )


func getlookup( node ):
	return Transform( node.get_global_transform().basis, Vector3( 0, 0, 0 ) ).translated( Vector3( 0, 1, 0 ) ).origin
