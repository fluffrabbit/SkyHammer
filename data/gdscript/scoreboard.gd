extends RichTextLabel


export var ScoreboardFile = 'scoreboard.json'
var filename = 'user://' + ScoreboardFile

export var PlayerName = 'me'

export var ThisTab = 1

var scoreboard = [
	[ 'V@RTEX',    9001 ],
	[ 'Sonichu',   5150 ],
	[ 'Killsbury', 2001 ],
	[ 'ImaPC',     2000 ],
	[ 'W@RL@R',    1337 ],
	[ 'HUFFYUV',    420 ],
	[ 'Snackable',  404 ],
	[ 'Ballin',     360 ],
	[ 'Killhouse',   86 ],
	[ 'Sublime',     69 ]
]


func _ready():
	var s = 'meta_clicked'
	if !is_connected( s, self, s ): connect( s, self, s )
	var f = File.new()
	if f.file_exists( filename ):
		var d = {}
		f.open( filename, File.READ )
		var r = d.parse_json( f.get_as_text() )
		f.close()
		if r == OK and d.has( 'scoreboard' ): scoreboard = d.scoreboard
	draw_scoreboard()


func meta_clicked( meta ):
	pass


func draw_scoreboard():
	var w = 16
	var bb = []
	for line in scoreboard:
		var color = color_seed( line[ 0 ].hash() ).linear_interpolate( Color( 1, 1, 1), 0.5 ).to_html( false )
		if line[ 0 ] == PlayerName: color = "FFFF00"
		bb.push_back( '[color=#' + color + ']' + line[ 0 ] + space( w - ( line[ 0 ].length() + str( line[ 1 ] ).length() ) ) + str( line[ 1 ] ) + '[/color]' )
	set_bbcode( '[code]' + join( bb, '\n' ) + '[/code]' )


func player_score( s ):
	get_node( 'sfx' ).play( 'click' )
	var i = 0
	for line in scoreboard:
		if s > line[ 1 ]:
			get_parent().set_current_tab( ThisTab )
			scoreboard.insert( i, [ PlayerName, s ] )
			scoreboard.resize( 10 )
			var d = {}
			d.scoreboard = scoreboard
			var f = File.new()
			f.open( filename, File.WRITE )
			f.store_string( d.to_json() )
			f.close()
			draw_scoreboard()
			return
		i += 1


# Helper functions

func join( array, connector = ',' ):
	var string = str( array[ 0 ] )
	if array.size() >= 2:
		for i in range( 1, array.size() ):
			string = string + connector + str( array[ i ] )
	return string


func space( count = 1, item = ' ' ):
	var string = ''
	for i in range( count ):
		string = string + item
	return string


func color_seed( s ):
	var r = rand_seed( s )
	var g = rand_seed( r[ 1 ] )
	var b = rand_seed( g[ 1 ] )
	var a = rand_seed( b[ 1 ] )
	return Color( ( r[ 0 ] % 256 ) / 255.0, ( g[ 0 ] % 256 ) / 255.0, ( b[ 0 ] % 256 ) / 255.0, ( a[ 0 ] % 256 ) / 255.0 )


func hash_color( color ):
	return Array( [ color.r, color.g, color.b, color.a ] ).hash()
