extends RigidBody

# 3D movement variables

export var movementSpeed = 5.0

export var mouseSensitivity = 0.25

export var forceAlwaysStrafe = true

onready var head = get_node( 'Head' )
onready var ray = head.get_node( 'RayCast' )
onready var panelM = head.get_node( 'PanelM' )
onready var panelL = head.get_node( 'PanelL' )
onready var panelR = head.get_node( 'PanelR' )
onready var mount = panelM.get_node( 'Viewport/Mount' )
onready var mount1 = panelL.get_node( 'Viewport/Mount' )
onready var mount2 = panelR.get_node( 'Viewport/Mount' )
onready var vignette = head.get_node( 'Vignette' )
onready var sensors = OS.get_name() == 'Android' or OS.get_name() == 'iOS'
onready var flipSensorAxis = OS.get_name() != 'Android'

var vr_is_set = false

var xsmooth = 0.24
var ysmooth = 0.184
var zsmooth = 0.15

var rthresh = 7.0
var rold = 0.0
var rnew = 0.0

var mouserotx = 0.0
var mouseroty = 0.0

var rotlower = PI * -0.5
var rotupper = PI * 0.5
var rollmax  = PI * 0.5

var rot = Vector3( 0, 0, 0 )

var accely = 0.0

var rotyoffsetkb = 0.0

var rotyoffsetmouse = 0.0

var rotymag = 0.0

var pitmode = false

var mv = Vector3( 0, 0, 0 )

var action = 0
var actionevent = null


# Initialization function

func _ready():
	if not vr_is_set: enable_vr( sensors )
	
	ray.set_enabled( true )
	ray.add_exception( self )
	Input.set_mouse_mode( Input.MOUSE_MODE_HIDDEN )
	Input.set_mouse_mode( Input.MOUSE_MODE_CAPTURED )
	set_fixed_process( true )
	set_process_input( true )


# Main loop

func _fixed_process( d ):
	var   up   = Input.is_action_pressed( 'ui_up' )
	var  down  = Input.is_action_pressed( 'ui_down' )
	var  left  = Input.is_action_pressed( 'ui_left' )
	var  right = Input.is_action_pressed( 'ui_right' )
	var  start = Input.is_action_pressed( 'ui_accept' )
	
	# rotate the body
	
	yxz_rotation( self, rot * Vector3( 0.0, 1.0, 0.0 ) )
	
	if sensors:
		# get the orientation of the player's head
		var dxs = d / xsmooth
		var dys = d / ysmooth
		var dzs = d / zsmooth
		
		var a = grav()
		var m = mag()
		
		var vec = rot
		
		if a == Vector3( 0.0, 0.0, 0.0 ):
			vec.x = mouserotx
			rotyoffsetmouse = mouseroty
		else:
			vec.z = rotslerp( vec.z, PI + atan2( a.x, a.y ), dzs )
			a = zRotateVector( a, vec.z )
			vec.x = rotslerp( vec.x, PI / 2 + atan2( a.y, a.z ), dxs )
			
			m = xRotateVector( zRotateVector( m, vec.z ), vec.x )
			
			# old project was getting 15 FPS, so for FRIM we set to that value
			
			var mult = d / 15.0
			
			rnew = atan2( m.x, m.z )
			var rdist = rotdist( rold, rnew )
			if abs( rdist ) < deg2rad( rthresh * 0.5 ) * mult:
				rnew = rdist * 0.04 + rold
			elif abs( rdist ) < deg2rad( rthresh ) * mult:
				rnew = rdist * 0.333 + rold
			elif abs( rdist ) < deg2rad( rthresh * 1.5 ) * mult:
				rnew = rdist * 0.667 + rold
			rotymag = rotslerp( rotymag, rnew, dys )
			rold = rnew
		
		
		vec.y = wrap( rotymag + rotyoffsetmouse )
		
		if vec.x < rotlower: vec.x = rotlower
		if vec.x > rotupper: vec.x = rotupper
		
		if vec.z > rollmax: vec.z = rollmax
		if vec.z < rollmax * -1.0: vec.z = rollmax * -1.0
		
		rot = vec
	
	# so now we're on to movement code
	
	var movex = 0.0
	
	if pitmode:
		movex = ( int( right ) - int( left ) ) * 0.7 * movementSpeed
		rotyoffsetkb = 0
	else:
		accely = slerp( ( int( right ) - int( left ) ) * 9.6 * d + accely, 0, d / ysmooth )
		rotyoffsetkb = wrap( accely * d + rotyoffsetkb )
	
	if sensors:
		rot.y = wrap( rot.y + rotyoffsetkb )
		yxz_rotation( head, rot * Vector3( 1.0, 0.0, 1.0 ) )
	
	mv = Vector3( movex, 0, ( int( up ) - int( down ) ) * -1.0 * movementSpeed )
	
	var globtrans = head.get_global_transform()
	mount.set_global_transform( globtrans )
	mount1.set_global_transform( globtrans )
	mount2.set_global_transform( globtrans )
	
	var movedist = sqrt( mv.x * mv.x + mv.z * mv.z )
	var moveang = wrap( atan2( mv.x, mv.z ) - rot.y )
	var lvel = Vector3( sin( moveang ) * movedist, mv.y, cos( moveang ) * movedist )
	var glvel = get_linear_velocity()
	set_linear_velocity( vlerp( get_linear_velocity(), Vector3( lvel.x, glvel.y, lvel.z ), d * 7.0 ) )
	
	if mv != Vector3( 0, 0, 0 ):
		# walking
		pass
	else:
		# not walking
		pass
	
	# special case for continuous press of start button
	if action == 0 and start: action = 2
	
	if ray.is_colliding():
		var target = ray.get_collider()
		if target != null and target.has_method( '_pick' ):
			target._pick( self, ray.get_collision_point(), action, actionevent, d )
	
	action = 0
	actionevent = null


func _input( e ):
	if e.type == InputEvent.MOUSE_MOTION:
		if sensors == false:
			mouserotx = wrap( deg2rad( e.relative_y * mouseSensitivity ) + mouserotx )
			mouseroty = wrap( deg2rad( e.relative_x * mouseSensitivity ) + mouseroty )
			if mouserotx < rotlower: mouserotx = rotlower
			if mouserotx > rotupper: mouserotx = rotupper
			rot.x = mouserotx
			rot.y = wrap( mouseroty + rotyoffsetkb )
			yxz_rotation( head, rot * Vector3( 1.0, 0.0, 1.0 ) )
	elif not e.type in [ InputEvent.SCREEN_DRAG, InputEvent.SCREEN_TOUCH ]:
		actionevent = e
		if e.is_pressed():
			if e.is_action( 'ui_accept' ):
				action = 1
			else:
				action = 3


func _pick( body, v, a, e, d ):
	# _pick( body, vec, action { point = 0, start press = 1, start hold = 2, button press = 3  }, actionevent, delta )
	# something looks at or interacts with the player
	pass


func is_vr():
	return not vignette.is_hidden()


func enable_vr( on ):
	vr_is_set = true
	if on:
		get_node( 'Head/PanelM' ).hide()
		get_node( 'Head/PanelL' ).show()
		get_node( 'Head/PanelR' ).show()
		get_node( 'Head/Vignette' ).show()
		# Disable FXAA (avoids double application bug in Godot 2.1)
		set_fxaa( get_node( 'Head/PanelL/Viewport/Mount/Camera' ), false )
		set_fxaa( get_node( 'Head/PanelR/Viewport/Mount/Camera' ), false )
		pitmode = Globals.get( 'pitmode' )
		if forceAlwaysStrafe: pitmode = true
	else:
		get_node( 'Head/PanelM' ).show()
		get_node( 'Head/PanelL' ).hide()
		get_node( 'Head/PanelR' ).hide()
		get_node( 'Head/Vignette' ).hide()
		# Enable FXAA
		set_fxaa( get_node( 'Head/PanelM/Viewport/Mount/Camera' ), true )
		pitmode = true


# Helper functions

func grav():
	var g = Input.get_gravity()
	if g.length() <= 0.1:
		g = Input.get_accelerometer()
	if flipSensorAxis:
		g = g * Vector3( -1.0, 1.0, -1.0 )
	return g


func mag():
	var m = Input.get_magnetometer()
	if flipSensorAxis:
		m = m * Vector3( -1.0, 1.0, -1.0 )
	return m


func yxz_rotation( node, r ):
	node.set_rotation( Vector3( 0, 0, 0 ) )
	node.rotate_y( r.y )
	node.rotate_x( r.x )
	node.rotate_z( r.z )


func slerp( v0, v1, t ):
	# Safe linear interpolation
	if t < 0: t = 0
	if t > 1: t = 1
	return lerp( v0, v1, t )


func rotslerp( v0, v1, t ):
	# Rotational and safe linear interpolation
	if t < 0: t = 0
	if t > 1: t = 1
	if v0 - v1 > PI:
		v0 -= PI + PI
	elif v1 - v0 > PI:
		v1 -= PI + PI
	return wrap( lerp( v0, v1, t ) )


func vlerp( aaa, bbb, f ):
	# Vector linear interpolation
	return Vector3( lerp( aaa.x, bbb.x, f ), lerp( aaa.y, bbb.y, f ), lerp( aaa.z, bbb.z, f ) )


func rotdist( v0, v1 ):
	if v0 - v1 > PI:
		v0 -= PI + PI
	elif v1 - v0 > PI:
		v1 -= PI + PI
	return v1 - v0


func xRotateVector( vct, angle ):
	var ss = sin( angle )
	var cc = cos( angle )
	return Vector3( vct.x, vct.y * cc - vct.z * ss, vct.y * ss + vct.z * cc )


func yRotateVector( vct, angle ):
	var ss = sin( angle )
	var cc = cos( angle )
	return Vector3( vct.x * cc - vct.z * ss, vct.y, vct.x * ss + vct.z * cc )


func zRotateVector( vct, angle ):
	var ss = sin( angle )
	var cc = cos( angle )
	return Vector3( vct.x * cc - vct.y * ss, vct.x * ss + vct.y * cc, vct.z )


func wrap( val ):
	var circ = PI + PI
	return val - ( int( val / circ ) * circ )


func quick_orientation():
	var a = grav()
	var m = mag()
	var v = Vector3( 0.0, 0.0, wrap( PI + atan2( a.x, a.y ) ) )
	a = zRotateVector( a, v.z )
	v.x = wrap( PI / 2 + atan2( a.y, a.z ) )
	m = xRotateVector( zRotateVector( m, v.z ), v.x )
	v.y = wrap( atan2( m.x, m.z ) )
	return v


func set_fxaa( cam, on ):
	var env = cam.get_environment()
	if not env: env = cam.get_world().get_environment()
	if env: env.set_enable_fx( 1, on )


func debug( text ):
	head.get_node( 'DebugOverlay' ).set_text( str( text ) )
