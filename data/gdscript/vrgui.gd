extends PhysicsBody


# THINGS THAT ARE STILL WRONG:
# * fabricated click event can't unfocus from text fields
# * can't select text by click and drag
# * can't suppress Android's on-screen keyboard


export( Texture ) var SelectCursor = preload( 'res://data/menu/select.png' )

onready var quad = get_node( 'Quad' )
onready var viewport = get_node( 'Viewport' )
onready var mat = quad.get_material_override()

# longest reasonable latency between _fixed_process and _process
var mousewait = 0.05
var mousetimer = mousewait
var mouse = null
var pos = null
var prev_pos = null


func _ready():
	# Update the render target once, then disable automatic render target updates.
	viewport.set_render_target_update_mode( 1 )
	mat.set_texture( 1, viewport.get_render_target_texture() )
	
	set_process( true )


func _process( d ):
	if mouse != null and not mouse.is_hidden():
		if mousetimer <= 0.0:
			mouse.hide()
			viewport.set_render_target_update_mode( 1 )
		mousetimer -= d


func _pick( body, v, a, e, d ):
	# _pick( body, vec, action { point = 0, start press = 1, start hold = 2, button press = 3  }, actionevent, delta )
	
	# UI interaction
	pos = get_global_transform().affine_inverse() * v
	pos = Vector2( pos.x, pos.y )
	# CollisionShape Nodes feed the parent CollisionObject Shapes. Here we are referencing a digested Shape, not a Node.
	# It might be better structure to reference a CollisionShape Node but Node lookups bloat the code.
	# In Godot 3.0 we will be able to use $ syntax and reference Nodes more directly like we are using the DOM.
	var extents = get_shape( 0 ).get_extents()
	pos.x = ( extents.x + pos.x ) * viewport.get_rect().size.x / ( extents.x * 2 )
	pos.y = ( extents.y - pos.y ) * viewport.get_rect().size.y / ( extents.y * 2 )
	
	# calculate relative_pos
	if prev_pos == null: prev_pos = pos
	var relative_pos = pos - prev_pos
	prev_pos = pos
	
	# send a preliminary MOUSE_MOTION event to ensure that everything gets passed
	var moe = InputEvent()
	moe.type = InputEvent.MOUSE_MOTION
	moe.ID = 1 # because magic numbers
	moe.pos = pos
	moe.global_pos = pos
	moe.relative_pos = relative_pos
	if mouse == null:
		mouse = Sprite.new()
		mouse.set_texture( SelectCursor )
		viewport.add_child( mouse )
	mousetimer = mousewait
	mouse.show()
	mouse.set_pos( pos )
	viewport.input( moe )
	
	# mouse clicking
	
	if e != null and e.type != InputEvent.MOUSE_MOTION:
		if e.type == InputEvent.JOYSTICK_BUTTON:
			# unable to unfocus from text fields for some reason
			var click = InputEvent()
			click.type = InputEvent.MOUSE_BUTTON
			click.ID = 1 # because magic numbers
			click.pos = pos
			click.button_mask = BUTTON_MASK_LEFT
			click.button_index = BUTTON_LEFT
			click.pressed = e.pressed
			viewport.input( click )
		else:
			if e.type == InputEvent.MOUSE_BUTTON:
				e.pos = pos
				e.global_pos = pos
				e.button_mask = BUTTON_MASK_LEFT
				e.button_index = BUTTON_LEFT
			viewport.input( e )
	
	viewport.set_render_target_update_mode( 1 )
