extends Button


export var SaveFile = 'vrmode.txt'
var filename = 'user://' + SaveFile

onready var player = get_tree().get_nodes_in_group( 'player' )[ 0 ]

# to fix Godot 2.1 particle speed bug
onready var particles = get_tree().get_nodes_in_group( 'particles' )

onready var vr = OS.get_name() == 'Android' or OS.get_name() == 'iOS'


func _ready():
	# to fix Godot 2.1 particle speed bug
	for p in particles:
		p.set_meta( 'originalLifetime', p.get_variable( 0 ) )
		p.set_meta( 'originalGravity', p.get_variable( 2 ) )
		p.set_meta( 'originalLinearVel', p.get_variable( 3 ) )
		p.set_meta( 'originalAngularVel', p.get_variable( 4 ) )
		p.set_meta( 'originalLinearAccel', p.get_variable( 5 ) )
		p.set_meta( 'originalTanAccel', p.get_variable( 7 ) )
	var f = File.new()
	if f.file_exists( filename ):
		f.open( filename, File.READ )
		vr = f.get_line() == 'True'
		f.close()
		player.enable_vr( vr )
	setlabel()
	# to fix Godot 2.1 particle speed bug
	stereo_particles( vr )


func _pressed():
	vr = not player.is_vr()
	var f = File.new()
	f.open( filename, File.WRITE )
	f.store_string( String( vr ) )
	f.close()
	player.enable_vr( vr )
	setlabel()
	# to fix Godot 2.1 particle speed bug
	stereo_particles( vr )


func setlabel():
	# This is where some syntactic sugar would help.
	# In JavaScript, it would be as simple as set_text( vr?'(((VR)))':'VR' )
	if vr:
		set_text( '(((VR)))' )
	else:
		set_text( 'VR' )


# to fix Godot 2.1 particle speed bug
func stereo_particles( on ):
	var m = 1.0
	if on: m = 0.5
	for p in particles:
		p.set_variable( 0, p.get_meta( 'originalLifetime' ) / m )
		p.set_variable( 2, p.get_meta( 'originalGravity' ) * m )
		p.set_variable( 3, p.get_meta( 'originalLinearVel' ) * m )
		p.set_variable( 4, p.get_meta( 'originalAngularVel' ) * m )
		p.set_variable( 5, p.get_meta( 'originalLinearAccel' ) * m )
		p.set_variable( 7, p.get_meta( 'originalTanAccel' ) * m )
