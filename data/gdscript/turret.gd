extends StaticBody


onready var head = get_node( 'Head' )
onready var gun_l = head.get_node( 'gun_l' )
onready var gun_r = head.get_node( 'gun_r' )
onready var shot_l = head.get_node( 'shot_l' )
onready var shot_r = head.get_node( 'shot_r' )
onready var ray_l = shot_l.get_node( 'RayCast' )
onready var ray_r = shot_r.get_node( 'RayCast' )
onready var sfx = head.get_node( 'sfx' )
onready var impact = get_node( 'impact' )
onready var player = get_tree().get_nodes_in_group( 'player' )[ 0 ]
onready var scoreboard = get_tree().get_nodes_in_group( 'scoreboard' )[ 0 ]

var alive = true

var score = 0

var commandRadius = 0.5

var rpm = 1800

var power = 15

var recoil = 0.055

var fire_timer = 0.0
var fire_side = 1

var gun_l_rot = 0.0
var gun_r_rot = 0.0

var impacting = false

var firing_l = false
var firing_r = false

var rot = Vector3( 0, 0, 0 )
var turret_rot = Vector3( 0, 0, 0 )

var x_min = -0.1
var x_max = PI * 0.3


func _ready():
	set_fixed_process( true )


func _fixed_process( d ):
	rotate_turret( d )
	
	gun_l_rot -= rpm / 120.0 * recoil * d
	if gun_l_rot < 0.0: gun_l_rot = 0.0
	yxz_rotation( gun_l, Vector3( gun_l_rot, 0, 0 ) )
	gun_r_rot -= rpm / 120.0 * recoil * d
	if gun_r_rot < 0.0: gun_r_rot = 0.0
	yxz_rotation( gun_r, Vector3( gun_r_rot, 0, 0 ) )
	
	impact.set_emitting( impacting )
	
	fire_timer -= d
	if fire_timer < 0:
		impacting = false
		fire_timer = 0
	
	if firing_l:
		shot_l.show()
		firing_l = false
	else:
		shot_l.hide()
	
	if firing_r:
		shot_r.show()
		firing_r = false
	else:
		shot_r.hide()
	
	if alive and ( get_global_transform().origin.distance_to( player.get_global_transform().origin ) > commandRadius ):
		die()
		respawn()


func _pick( body, v, a, e, d ):
	# _pick( body, vec, action { point = 0, start press = 1, start hold = 2, button press = 3  }, actionevent, delta )
	
	var bodylook = getlookat( body )
	var headlook = bodylook
	if body.has_node( 'Head' ): headlook = getlookat( body.get_node( 'Head' ) )
	rot.x = atan2( headlook.y * -1, sqrt( headlook.x * headlook.x + headlook.z * headlook.z ) )
	rot.y = atan2( bodylook.z, bodylook.x ) + PI / 2
	if rot.x < x_min: rot.x = x_min
	if rot.x > x_max: rot.x = x_max
	
	if alive and ( is_mouse_pressed() or is_joy_pressed() ):
		rotate_turret( d )
		if fire_timer <= 0:
			sfx.play( 'bang' )
			fire_timer = 60.0 / rpm
			if fire_side == 0:
				fire( gun_l, shot_l, ray_l )
				gun_l_rot = recoil
				firing_l = true
				fire_side = 1
			else:
				fire( gun_r, shot_r, ray_r )
				gun_r_rot = recoil
				firing_r = true
				fire_side = 0


# Functions specific to this node

func fire( gun, shot, ray ):
	yxz_rotation( gun, Vector3( recoil, 0, 0 ) )
	yxz_rotation( shot, Vector3( recoil * randf(), recoil * randf() - recoil * 0.5, 0 ) )
	
	ray.force_raycast_update()
	if ray.is_colliding():
		impacting = true
		var hitpoint = ray.get_collision_point()
		impact.set_global_transform( Transform().translated( hitpoint ) )
		
		var obj = ray.get_collider()
		if obj.has_method( 'apply_impulse' ):
			obj.apply_impulse( hitpoint, getlookat( ray ) * power )
		if obj.has_method( '_pick' ):
			score += 1
			obj._pick( self, hitpoint, 10, null, 0.0 )
	else:
		impacting = false


func rotate_turret( d ):
	turret_rot.x = rotslerp( turret_rot.x, rot.x, 10 * d )
	turret_rot.y = rotfollow( turret_rot.y, rot.y, PI * 1.5 * d )
	yxz_rotation( self, turret_rot * Vector3( 0, 1, 0 ) )
	yxz_rotation( head, turret_rot * Vector3( 1, 0, 0 ) )


func die():
	if scoreboard != null and scoreboard.has_method( 'player_score' ):
		scoreboard.player_score( score )
	score = 0
	alive = false


func respawn():
	player.set_linear_velocity( Vector3( 0, 0, 0 ) )
	player.set_global_transform( Transform( player.get_global_transform().basis, get_global_transform().origin ) )
	alive = true


# Helper functions

func is_mouse_pressed():
	for m in range( 1, 3 ):
		if Input.is_mouse_button_pressed( m ): return true
	return false


func is_joy_pressed():
	for j in Input.get_connected_joysticks():
		for b in range( 0, 16 ):
			if Input.is_joy_button_pressed( j, b ): return true
	return false


func getlookat( node ):
	return Transform( node.get_global_transform().basis, Vector3( 0, 0, 0 ) ).translated( Vector3( 0, 0, 1 ) ).origin


func yxz_rotation( node, r ):
	node.set_rotation( Vector3( 0, 0, 0 ) )
	node.rotate_y( r.y )
	node.rotate_x( r.x )
	node.rotate_z( r.z )


func slerp( v0, v1, t ):
	# Safe linear interpolation
	if t < 0: t = 0
	if t > 1: t = 1
	return lerp( v0, v1, t )


func rotslerp( v0, v1, t ):
	# Rotational and safe linear interpolation
	if t < 0: t = 0
	if t > 1: t = 1
	if v0 - v1 > PI:
		v0 -= PI + PI
	elif v1 - v0 > PI:
		v1 -= PI + PI
	return wrap( lerp( v0, v1, t ) )


func vlerp( aaa, bbb, f ):
	# Vector linear interpolation
	return Vector3( lerp( aaa.x, bbb.x, f ), lerp( aaa.y, bbb.y, f ), lerp( aaa.z, bbb.z, f ) )


func rotdist( v0, v1 ):
	if v0 - v1 > PI:
		v0 -= PI + PI
	elif v1 - v0 > PI:
		v1 -= PI + PI
	return v1 - v0


func rotfollow( v0, v1, s ):
	return rotslerp( v0, v1, abs( s / rotdist( v0, v1 ) ) )


func xRotateVector( vct, angle ):
	var ss = sin( angle )
	var cc = cos( angle )
	return Vector3( vct.x, vct.y * cc - vct.z * ss, vct.y * ss + vct.z * cc )


func yRotateVector( vct, angle ):
	var ss = sin( angle )
	var cc = cos( angle )
	return Vector3( vct.x * cc - vct.z * ss, vct.y, vct.x * ss + vct.z * cc )


func zRotateVector( vct, angle ):
	var ss = sin( angle )
	var cc = cos( angle )
	return Vector3( vct.x * cc - vct.y * ss, vct.x * ss + vct.y * cc, vct.z )


func wrap( val ):
	var circ = PI + PI
	return val - ( int( val / circ ) * circ )
