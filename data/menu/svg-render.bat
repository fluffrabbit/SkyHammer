@echo off

%= Render all of the SVGs to PNGs. =%

for /r %%f in (*.svg) do (
	inkscape -z -e "%%~nf" "%%f"
)
