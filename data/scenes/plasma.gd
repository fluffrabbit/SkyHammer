extends RigidBody


onready var orb = get_node( 'Orb' )


var countdown = 2
var firstrun = true


func _ready():
	set_process( true )


func _process( d ):
	if not is_hidden():
		orb.set_rotation( Vector3( randf() * PI * 2.0, randf() * PI * 2.0, randf() * PI * 2.0 ) )
		if firstrun:
			get_node( 'sfx' ).play( 'plasma' )
			set_fixed_process( true )
			firstrun = false


func _fixed_process( d ):
	countdown -= d
	if countdown <= 0:
		get_tree().queue_delete( self )
		get_parent().remove_child( self )
