The main repository is now hosted here: https://rocketgit.com/user/fluffrabbit/SkyHammer

# Sky Hammer

Aliens came. Everything was blown up. But then one day when you were scavenging for food, you found the ultimate weapon. The Sky Hammer is a twin barrel turret that fires 1800 caseless 7mm rounds per minute, designed for one purpose: sending aliens love letters. You are the postmaster.

You can't walk around (yet!) because joystick movement is currently unimplemented, but you can shoot.

Recommended specs for gameplay: Android 4.0+ or iOS device with 4.5" - 6" screen OR dedicated Android VR headset. Device must have a magnetometer. Bluetooth controller highly recommended.

## Features

* Godot engine 2.1 compatible
* Virtual interactive display
* 3D sound
* Extensive use of 3D physics

## Development

Recommended specs for development: Linux or Mac desktop with working Android build chain (Android/Replicant SDK, OpenJDK, Godot, all good software to have). Windows should work as well but Linux is the easiest platform for development when dealing with Android.

Sky Hammer currently implements its own HMD compatibility (Godot's first in 2.1). However, a lot has changed since this project was started and Godot 3.0 is approaching stability with built-in HMD support. Sky Hammer should be adapted accordingly. Soon.

Download or clone the repository and import the project into Godot. When you want to push changes to the repository, an easy way to do it is with the script. Run `./git-upload` on *nix or `git-upload.bat` on Windows. If you are not a member of the main Sky Hammer repository, you can set up your own and change the Git URL in the scripts accordingly.
